<?php

namespace App\Entity\Organisasi;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use ApiPlatform\Core\Annotation\ApiResource;
//use App\Entity\Core\Role;
//use App\Entity\Pegawai\JabatanPegawai;
use App\Repository\Organisasi\KantorRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use Symfony\Bridge\Doctrine\IdGenerator\UuidV4Generator;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=KantorRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="kantor", indexes={
 *     @ORM\Index(name="idx_kantor_nama_status", columns={"id", "nama", "level", "sk"}),
 *     @ORM\Index(name="idx_kantor_legacy", columns={"id", "legacy_kode", "legacy_kode_kpp", "legacy_kode_kanwil"}),
 *     @ORM\Index(name="idx_kantor_relation", columns={"id", "jenis_kantor_id", "parent_id", "level", "pembina_id"}),
 *     @ORM\Index(name="idx_kantor_location", columns={"id", "latitude", "longitude"}),
 * })
 * Disable second level cache for further analysis
 * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
 */
#[ApiResource(
//    collectionOperations: [
//        'get' => [
//            'security' => 'is_granted("ROLE_USER")',
//            'security_message' => 'Only a valid user can access this.'
//        ],
//        'post' => [
//            'security'=>'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
//            'security_message'=>'Only admin/app can add new resource to this entity type.'
//        ]
//    ],
//    itemOperations: [
//        'get' => [
//            'security' => 'is_granted("ROLE_USER")',
//            'security_message' => 'Only a valid user can access this.'
//        ],
//        'put' => [
//            'security' => 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
//            'security_message' => 'Only admin/app can add new resource to this entity type.'
//        ],
//        'patch' => [
//            'security' => 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
//            'security_message' => 'Only admin/app can add new resource to this entity type.'
//        ],
//        'delete' => [
//            'security' => 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
//            'security_message' => 'Only admin/app can add new resource to this entity type.'
//        ],
//    ],
//    attributes: [
//        'security' => 'is_granted("ROLE_USER")',
//        'security_message' => 'Only a valid user can access this.',
//        'order' => [
//            'level' => 'ASC',
//            'nama' => 'ASC'
//        ]
//    ],
    mercure: true,
)]
#[ApiFilter(SearchFilter::class, properties: [
    'nama' => 'ipartial',
    'sk' => 'ipartial',
    'legacyKode' => 'partial',
    'legacyKodeKpp' => 'partial',
    'legacyKodeKanwil' => 'partial'
])]
#[ApiFilter(DateFilter::class, properties: ['tanggalAktif', 'tanggalNonaktif'])]
#[ApiFilter(NumericFilter::class, properties: ['level'])]
#[ApiFilter(PropertyFilter::class)]
class Kantor
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidV4Generator::class)
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
//     * @Groups({"pegawai:read"})
//     * @Groups({"user:read"})
     */
    #[Assert\NotBlank]
    private ?string $nama;

    /**
     * @ORM\ManyToOne(targetEntity=JenisKantor::class, inversedBy="kantors")
     * @ORM\JoinColumn(nullable=false)
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    #[Assert\NotNull]
    #[Assert\Valid]
    private $jenisKantor;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    private ?int $level;

    /**
     * @ORM\ManyToOne(targetEntity=Kantor::class, inversedBy="childs")
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    #[Assert\Valid]
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity=Kantor::class, mappedBy="parent")
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    private $childs;

    /**
     * @ORM\Column(type="datetime_immutable")
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    #[Assert\NotNull]
    private ?DateTimeImmutable $tanggalAktif;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    private ?DateTimeImmutable $tanggalNonaktif;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    private ?string $sk;

    /**
     * @ORM\Column(type="text", nullable=true)
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    private ?string $alamat;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    private ?string $telp;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    private ?string $fax;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    private ?string $zonaWaktu;

    /**
     * @ORM\Column(type="float", nullable=true)
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    private ?float $latitude;

    /**
     * @ORM\Column(type="float", nullable=true)
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    private ?float $longitude;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    private ?string $legacyKode;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    private ?string $legacyKodeKpp;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    private ?string $legacyKodeKanwil;

//    /**
//     * @ORM\OneToMany(targetEntity=JabatanPegawai::class, mappedBy="kantor", orphanRemoval=true)
//     * Disable second level cache for further analysis
//     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
//     */
//    private $jabatanPegawais;
//
//    /**
//     * @ORM\ManyToMany(targetEntity=Role::class, mappedBy="kantors")
//     */
//    private $roles;

    /**
     * @ORM\ManyToOne(targetEntity=Kantor::class, inversedBy="membina")
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    private $pembina;

    /**
     * @ORM\OneToMany(targetEntity=Kantor::class, mappedBy="pembina")
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    private $membina;

    #[Pure] public function __construct()
    {
        $this->childs = new ArrayCollection();
//        $this->jabatanPegawais = new ArrayCollection();
//        $this->roles = new ArrayCollection();
        $this->membina = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->nama;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNama(): ?string
    {
        return $this->nama;
    }

    public function setNama(string $nama): self
    {
        $this->nama = $nama;

        return $this;
    }

    public function getJenisKantor(): ?JenisKantor
    {
        return $this->jenisKantor;
    }

    public function setJenisKantor(?JenisKantor $jenisKantor): self
    {
        $this->jenisKantor = $jenisKantor;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(?int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getChilds(): Collection|array
    {
        return $this->childs;
    }

    public function addChild(self $child): self
    {
        if (!$this->childs->contains($child)) {
            $this->childs[] = $child;
            $child->setParent($this);
        }

        return $this;
    }

    public function removeChild(self $child): self
    {
        if ($this->childs->removeElement($child)) {
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }

    public function getTanggalAktif(): ?DateTimeImmutable
    {
        return $this->tanggalAktif;
    }

    public function setTanggalAktif(DateTimeImmutable $tanggalAktif): self
    {
        $this->tanggalAktif = $tanggalAktif;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setTanggalAktifValue(): void
    {
        $this->tanggalAktif = new DateTimeImmutable();
    }

    public function getTanggalNonaktif(): ?DateTimeImmutable
    {
        return $this->tanggalNonaktif;
    }

    public function setTanggalNonaktif(?DateTimeImmutable $tanggalNonaktif): self
    {
        $this->tanggalNonaktif = $tanggalNonaktif;

        return $this;
    }

    public function getSk(): ?string
    {
        return $this->sk;
    }

    public function setSk(?string $sk): self
    {
        $this->sk = $sk;

        return $this;
    }

    public function getAlamat(): ?string
    {
        return $this->alamat;
    }

    public function setAlamat(?string $alamat): self
    {
        $this->alamat = $alamat;

        return $this;
    }

    public function getTelp(): ?string
    {
        return $this->telp;
    }

    public function setTelp(?string $telp): self
    {
        $this->telp = $telp;

        return $this;
    }

    public function getFax(): ?string
    {
        return $this->fax;
    }

    public function setFax(?string $fax): self
    {
        $this->fax = $fax;

        return $this;
    }

    public function getZonaWaktu(): ?string
    {
        return $this->zonaWaktu;
    }

    public function setZonaWaktu(?string $zonaWaktu): self
    {
        $this->zonaWaktu = $zonaWaktu;

        return $this;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(?float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(?float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getLegacyKode(): ?string
    {
        return $this->legacyKode;
    }

    public function setLegacyKode(?string $legacyKode): self
    {
        $this->legacyKode = $legacyKode;

        return $this;
    }

    public function getLegacyKodeKpp(): ?string
    {
        return $this->legacyKodeKpp;
    }

    public function setLegacyKodeKpp(?string $legacyKodeKpp): self
    {
        $this->legacyKodeKpp = $legacyKodeKpp;

        return $this;
    }

    public function getLegacyKodeKanwil(): ?string
    {
        return $this->legacyKodeKanwil;
    }

    public function setLegacyKodeKanwil(?string $legacyKodeKanwil): self
    {
        $this->legacyKodeKanwil = $legacyKodeKanwil;

        return $this;
    }

//    /**
//     * @return Collection|JabatanPegawai[]
//     */
//    public function getJabatanPegawais(): Collection|array
//    {
//        return $this->jabatanPegawais;
//    }
//
//    public function addJabatanPegawai(JabatanPegawai $jabatanPegawai): self
//    {
//        if (!$this->jabatanPegawais->contains($jabatanPegawai)) {
//            $this->jabatanPegawais[] = $jabatanPegawai;
//            $jabatanPegawai->setKantor($this);
//        }
//
//        return $this;
//    }
//
//    public function removeJabatanPegawai(JabatanPegawai $jabatanPegawai): self
//    {
//        if ($this->jabatanPegawais->contains($jabatanPegawai)) {
//            $this->jabatanPegawais->removeElement($jabatanPegawai);
//            // set the owning side to null (unless already changed)
//            if ($jabatanPegawai->getKantor() === $this) {
//                $jabatanPegawai->setKantor(null);
//            }
//        }
//
//        return $this;
//    }
//
//    /**
//     * @return Collection|Role[]
//     */
//    public function getRoles(): Collection|array
//    {
//        return $this->roles;
//    }
//
//    public function addRole(Role $role): self
//    {
//        if (!$this->roles->contains($role)) {
//            $this->roles[] = $role;
//        }
//
//        return $this;
//    }
//
//    public function removeRole(Role $role): self
//    {
//        if ($this->roles->contains($role)) {
//            $this->roles->removeElement($role);
//        }
//
//        return $this;
//    }

    public function getPembina(): ?self
    {
        return $this->pembina;
    }

    public function setPembina(?self $pembina): self
    {
        $this->pembina = $pembina;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getMembina(): Collection|array
    {
        return $this->membina;
    }

    public function addMembina(self $membina): self
    {
        if (!$this->membina->contains($membina)) {
            $this->membina[] = $membina;
            $membina->setPembina($this);
        }

        return $this;
    }

    public function removeMembina(self $membina): self
    {
        if ($this->membina->removeElement($membina)) {
            // set the owning side to null (unless already changed)
            if ($membina->getPembina() === $this) {
                $membina->setPembina(null);
            }
        }

        return $this;
    }
}
