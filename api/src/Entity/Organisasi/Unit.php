<?php

namespace App\Entity\Organisasi;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use ApiPlatform\Core\Annotation\ApiResource;
//use App\Entity\Core\Role;
//use App\Entity\Pegawai\JabatanPegawai;
use App\Repository\Organisasi\UnitRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use Symfony\Bridge\Doctrine\IdGenerator\UuidV4Generator;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UnitRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="unit", indexes={
 *     @ORM\Index(name="idx_unit_nama", columns={"id", "nama", "level"}),
 *     @ORM\Index(name="idx_unit_legacy", columns={"id", "legacy_kode", "pembina_id"}),
 *     @ORM\Index(name="idx_unit_relation", columns={"id", "jenis_kantor_id", "parent_id", "eselon_id"}),
 * })
 * Disable second level cache for further analysis
 * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
 */
#[ApiResource(
//    collectionOperations: [
//        'get' => [
//            'security' => 'is_granted("ROLE_USER")',
//            'security_message' => 'Only a valid user can access this.'
//        ],
//        'post' => [
//            'security'=>'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
//            'security_message'=>'Only admin/app can add new resource to this entity type.'
//        ]
//    ],
//    itemOperations: [
//        'get' => [
//            'security' => 'is_granted("ROLE_USER")',
//            'security_message' => 'Only a valid user can access this.'
//        ],
//        'put' => [
//            'security' => 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
//            'security_message' => 'Only admin/app can add new resource to this entity type.'
//        ],
//        'patch' => [
//            'security' => 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
//            'security_message' => 'Only admin/app can add new resource to this entity type.'
//        ],
//        'delete' => [
//            'security' => 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
//            'security_message' => 'Only admin/app can add new resource to this entity type.'
//        ],
//    ],
//    attributes: [
//        'security' => 'is_granted("ROLE_USER")',
//        'security_message' => 'Only a valid user can access this.',
//        'order' => [
//            'level' => 'ASC',
//            'nama' => 'ASC'
//        ]
//    ],
    mercure: true,
)]
#[ApiFilter(SearchFilter::class, properties: [
    'nama' => 'ipartial',
    'legacyKode' => 'partial',
])]
#[ApiFilter(DateFilter::class, properties: ['tanggalAktif', 'tanggalNonaktif'])]
#[ApiFilter(NumericFilter::class, properties: ['level'])]
#[ApiFilter(PropertyFilter::class)]
class Unit
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidV4Generator::class)
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
//     * @Groups({"pegawai:read"})
     */
    #[Assert\NotBlank]
    private ?string $nama;

    /**
     * @ORM\ManyToOne(targetEntity=JenisKantor::class, inversedBy="units")
     * @ORM\JoinColumn(nullable=false)
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    #[Assert\NotNull]
    #[Assert\Valid]
    private $jenisKantor;

    /**
     * @ORM\Column(type="integer")
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
//     * @Groups({"pegawai:read"})
     */
    #[Assert\NotNull]
    private ?int $level;

    /**
     * @ORM\ManyToOne(targetEntity=Unit::class, inversedBy="childs")
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    #[Assert\Valid]
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity=Unit::class, mappedBy="parent")
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    private $childs;

    /**
     * @ORM\ManyToOne(targetEntity=Eselon::class, inversedBy="units")
     * @ORM\JoinColumn(nullable=false)
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    #[Assert\NotNull]
    #[Assert\Valid]
    private $eselon;

    /**
     * @ORM\Column(type="datetime_immutable")
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    #[Assert\NotNull]
    private ?DateTimeImmutable $tanggalAktif;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    private ?DateTimeImmutable $tanggalNonaktif;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    private ?string $legacyKode;

//    /**
//     * @ORM\OneToMany(targetEntity=JabatanPegawai::class, mappedBy="unit")
//     * Disable second level cache for further analysis
//     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
//     */
//    private $jabatanPegawais;

    /**
     * @ORM\ManyToMany(targetEntity=Jabatan::class, mappedBy="units")
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    private $jabatans;

//    /**
//     * @ORM\ManyToMany(targetEntity=Role::class, mappedBy="units")
//     */
//    private $roles;

    /**
     * @ORM\ManyToOne(targetEntity=Unit::class, inversedBy="membina")
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    private $pembina;

    /**
     * @ORM\OneToMany(targetEntity=Unit::class, mappedBy="pembina")
     * Disable second level cache for further analysis
     * @ ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    private $membina;

    #[Pure] public function __construct()
    {
        $this->childs = new ArrayCollection();
//        $this->jabatanPegawais = new ArrayCollection();
        $this->jabatans = new ArrayCollection();
//        $this->roles = new ArrayCollection();
        $this->membina = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->nama;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNama(): ?string
    {
        return $this->nama;
    }

    public function setNama(string $nama): self
    {
        $this->nama = $nama;

        return $this;
    }

    public function getJenisKantor(): ?JenisKantor
    {
        return $this->jenisKantor;
    }

    public function setJenisKantor(?JenisKantor $jenisKantor): self
    {
        $this->jenisKantor = $jenisKantor;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getChilds(): Collection|array
    {
        return $this->childs;
    }

    public function addChild(self $child): self
    {
        if (!$this->childs->contains($child)) {
            $this->childs[] = $child;
            $child->setParent($this);
        }

        return $this;
    }

    public function removeChild(self $child): self
    {
        if ($this->childs->removeElement($child)) {
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }

    public function getEselon(): ?Eselon
    {
        return $this->eselon;
    }

    public function setEselon(?Eselon $eselon): self
    {
        $this->eselon = $eselon;

        return $this;
    }

    public function getTanggalAktif(): ?DateTimeImmutable
    {
        return $this->tanggalAktif;
    }

    public function setTanggalAktif(DateTimeImmutable $tanggalAktif): self
    {
        $this->tanggalAktif = $tanggalAktif;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setTanggalAktifValue(): void
    {
        $this->tanggalAktif = new DateTimeImmutable();
    }

    public function getTanggalNonaktif(): ?DateTimeImmutable
    {
        return $this->tanggalNonaktif;
    }

    public function setTanggalNonaktif(?DateTimeImmutable $tanggalNonaktif): self
    {
        $this->tanggalNonaktif = $tanggalNonaktif;

        return $this;
    }

    public function getLegacyKode(): ?string
    {
        return $this->legacyKode;
    }

    public function setLegacyKode(?string $legacyKode): self
    {
        $this->legacyKode = $legacyKode;

        return $this;
    }

//    /**
//     * @return Collection|JabatanPegawai[]
//     */
//    public function getJabatanPegawais(): Collection|array
//    {
//        return $this->jabatanPegawais;
//    }
//
//    public function addJabatanPegawai(JabatanPegawai $jabatanPegawai): self
//    {
//        if (!$this->jabatanPegawais->contains($jabatanPegawai)) {
//            $this->jabatanPegawais[] = $jabatanPegawai;
//            $jabatanPegawai->setUnit($this);
//        }
//
//        return $this;
//    }
//
//    public function removeJabatanPegawai(JabatanPegawai $jabatanPegawai): self
//    {
//        if ($this->jabatanPegawais->contains($jabatanPegawai)) {
//            $this->jabatanPegawais->removeElement($jabatanPegawai);
//            // set the owning side to null (unless already changed)
//            if ($jabatanPegawai->getUnit() === $this) {
//                $jabatanPegawai->setUnit(null);
//            }
//        }
//
//        return $this;
//    }

    /**
     * @return Collection|Jabatan[]
     */
    public function getJabatans(): Collection|array
    {
        return $this->jabatans;
    }

    public function addJabatan(Jabatan $jabatan): self
    {
        if (!$this->jabatans->contains($jabatan)) {
            $this->jabatans[] = $jabatan;
            $jabatan->addUnit($this);
        }

        return $this;
    }

    public function removeJabatan(Jabatan $jabatan): self
    {
        if ($this->jabatans->contains($jabatan)) {
            $this->jabatans->removeElement($jabatan);
            $jabatan->removeUnit($this);
        }

        return $this;
    }

//    /**
//     * @return Collection|Role[]
//     */
//    public function getRoles(): Collection|array
//    {
//        return $this->roles;
//    }
//
//    public function addRole(Role $role): self
//    {
//        if (!$this->roles->contains($role)) {
//            $this->roles[] = $role;
//        }
//
//        return $this;
//    }
//
//    public function removeRole(Role $role): self
//    {
//        if ($this->roles->contains($role)) {
//            $this->roles->removeElement($role);
//        }
//
//        return $this;
//    }

    public function getPembina(): ?self
    {
        return $this->pembina;
    }

    public function setPembina(?self $pembina): self
    {
        $this->pembina = $pembina;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getMembina(): Collection
    {
        return $this->membina;
    }

    public function addMembina(self $membina): self
    {
        if (!$this->membina->contains($membina)) {
            $this->membina[] = $membina;
            $membina->setPembina($this);
        }

        return $this;
    }

    public function removeMembina(self $membina): self
    {
        if ($this->membina->removeElement($membina)) {
            // set the owning side to null (unless already changed)
            if ($membina->getPembina() === $this) {
                $membina->setPembina(null);
            }
        }

        return $this;
    }
}
